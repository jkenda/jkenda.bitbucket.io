
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function kreirajEHRzaBolnika(ime, priimek, datumRojstva) {
 		$.ajax({
       url: baseUrl + "/ehr",
       type: 'POST',
       headers: {
         "Authorization": getAuthorization()
       },
       success: function (data) {
         var ehrId = data.ehrId;
         var partyData = {
           firstNames: ime,
           lastNames: priimek,
           dateOfBirth: datumRojstva,
           additionalInfo: {"ehrId": ehrId}
         };
         $.ajax({
           url: baseUrl + "/demographics/party",
           type: 'POST',
           headers: {
             "Authorization": getAuthorization()
           },
           contentType: 'application/json',
           data: JSON.stringify(partyData),
           success: function (party) {
             if (party.action == 'CREATE') {
               $("#izbira-osebe").append(
                 "<li><a href='#'>" + ime + " " + priimek + "</a><span class='ehrId' style='display: none;'>" + ehrId + "</span></li>"
               );
             }
           }
         });
       }
 		});
 }

function generirajPodatke() {
  /* kreiraj nove uporabnike */
  $("#izbira-osebe").html("");
  kreirajEHRzaBolnika("Jan", "Travnik", "1990-02-23");
  kreirajEHRzaBolnika("Primož", "Krof", "1989-10-29");
  kreirajEHRzaBolnika("Nina", "Piflar", "1988-10-25");

  /* listenerji za izbor osebe */
  $(document).on("click", "#izbira-osebe li", function() {
    $("#gumbZaOsebo").html("Izbrana oseba: " + $(this).find("a").text() + " <span class='caret'>");
    ehrId = $(this).find(".ehrId").text();
    preberiMeritveVitalnihZnakov(function(podatki) {
      if (bmiFormat.stevilke.length > 0) narisiGraf();
      prikaziPrikaz();
    });
  });
}



const MEJA_PREKOMERNE_TEZE = 25;
const MEJA_PRENIZKE_TEZE   = 18.5;
const MEJA_DEBELOSTI       = 40;
var kategorijaTeze = 1; // 0: prenizka, 1: normalna, 2: prekomerna, 3: debelost

var ehrId;
var visinaFormat = {
  stevilke: [],
  datum: []
};
var tezaFormat = {
  stevilke: [],
  datum: []
};
var bmiFormat = {
  stevilke: [],
  datum: []
};

function prikaziDodajanje() {
  $(".prikaz-podatkov").hide();
  $(".panel-body").show();
  $(".prikazi-podatke").show();
}

function prikaziPrikaz() {
  if (bmiFormat.stevilke.length == 0) prikaziDodajanje();
  else {
    $(".prikaz-podatkov").show();
    $(".panel-body").hide();
    $(".prikazi-podatke").show();
  }
}

function dodajMeritveVitalnihZnakov() {
  var d = new Date();
	var datumInUra = d.getFullYear() + "-" +
  (d.getMonth() > 9 ? (d.getMonth() + 1) : "0" + (d.getMonth() + 1)) + "-" +
  (d.getDate() > 9 ? (d.getDate() + 1) : "0" + (d.getDate() + 1)) + "T" +
  (d.getHours() > 9 ? (d.getHours() + 1) : "0" + (d.getHours() + 1)) + ":" +
  (d.getMinutes() > 9 ? (d.getMinutes() + 1) : "0" + (d.getMinutes() + 1)) + "Z";

	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
  if (telesnaVisina == "") telesnaVisina = visinaFormat.stevilke[visinaFormat.stevilke.length - 1];
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  if (telesnaTeza == "") telesnaTeza = tezaFormat.stevilke[tezaFormat.stevilke.length - 1];
  var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
  var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	if (!sistolicniKrvniTlak  || sistolicniKrvniTlak.trim().length  == 0 ||
      !diastolicniKrvniTlak || diastolicniKrvniTlak.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim, vnesite krvni tlak!</span>");
    $("#dodajMeritveVitalnihZnakovSporocilo").show();
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "eZdravje"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
  $("#dodajVitalnoTelesnaVisina").val("");
  $("#dodajVitalnoTelesnaTeza").val("");
  $("#dodajVitalnoKrvniTlakSistolicni").val("");
  $("#dodajVitalnoKrvniTlakDiastolicni").val("");
  preberiMeritveVitalnihZnakov(function(podatki) {
    narisiGraf();
  });
}

function pridobiPodatke(tip, callback) {
  var podatki;
  $.ajax({ // tip
    url: baseUrl + "/view/" + ehrId + "/" + tip,
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      if (res.length > 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").text("");
        $("#preberiMeritveVitalnihZnakovSporocilo").hide();
      } else {
        $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-warning fade-in'>" +
          "Uporabnik nima nobenega podatka.</span>");
        $("#preberiMeritveVitalnihZnakovSporocilo").show();
      }
      callback(res);
    },
    error: function() {
      $("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
        $("#preberiMeritveVitalnihZnakovSporocilo").show();
    }
  });
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
function preberiMeritveVitalnihZnakov(callback) {
	var tip = $("#preberiTipZaVitalneZnake").val();

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
        pridobiPodatke("height", function(visina) {
          pridobiPodatke("weight", function(teza) {
            visinaFormat.stevilke = []; visinaFormat.datum = [];
            tezaFormat.stevilke = []; tezaFormat.datum = [];
            bmiFormat.stevilke = []; bmiFormat.datum = [];
            for (var i = 0; i < visina.length; i++) {
              visinaFormat.stevilke[i] = visina[visina.length - i - 1].height;
              visinaFormat.datum[i]    = visina[visina.length - i - 1].time.split("T")[0] + " " + visina[i].time.split("T")[1].split("Z")[0];

              tezaFormat.stevilke[i] = teza[visina.length - i - 1].weight;
              tezaFormat.datum[i]    = teza[visina.length - i - 1].time.split("T")[0] + " " + visina[i].time.split("T")[1].split("Z")[0];

              bmiFormat.stevilke[i] = tezaFormat.stevilke[i] / (visinaFormat.stevilke[i] * visinaFormat.stevilke[i] / 10000);
              bmiFormat.datum[i]  = visina[i].time.split("T")[0] + " " + visina[i].time.split("T")[1].split("Z")[0];
            }

            var zadnjiBmi = bmiFormat.stevilke[bmiFormat.stevilke.length - 1];
            if (zadnjiBmi < MEJA_PRENIZKE_TEZE) kategorijaTeze = 0;
            else if (zadnjiBmi >= MEJA_DEBELOSTI) {
              kategorijaTeze = 3;

            }
            else if (zadnjiBmi >= MEJA_PREKOMERNE_TEZE) kategorijaTeze = 2;
            else kategorijaTeze = 1;

            switch (kategorijaTeze) {
              case 0:
                $("#prenizkaTeza").show();
                $("#normalnaTeza").hide();
                $("#prekomernaTeza").hide();
                $("#debelost").hide();
                break;
              case 1:
              $("#prenizkaTeza").hide();
              $("#normalnaTeza").show();
              $("#prekomernaTeza").hide();
              $("#debelost").hide();
                break;
              case 2:
                $("#prenizkaTeza").hide();
                $("#normalnaTeza").hide();
                $("#prekomernaTeza").show();
                $("#debelost").hide();
                break;
              case 3:
              $("#prenizkaTeza").hide();
              $("#normalnaTeza").hide();
              $("#prekomernaTeza").hide();
              $("#debelost").show();
                break;
            }

            callback ({
              visina: visinaFormat,
              teza: tezaFormat,
              bmi: bmiFormat
            });
          });
        });
    	},
    	error: function(err) {
    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          $("#preberiMeritveVitalnihZnakovSporocilo").show();
          $(".prikazi-podatke").show();
          return err;
    	}
	});
}

function narisiGraf(podatki) {
  var ctx = document.getElementById('graf').getContext('2d');
  var graf = new Chart(ctx, {
    type: 'line',
    data: {
      labels: bmiFormat.datum,
      datasets: [{
        label: "ITM",
        fill: false,
        borderColor: '#337ab7',
        data: bmiFormat.stevilke // podatki.bmi.stevilke
      },
      {
        label: "Prenizka teža",
        pointRadius: 0,
        borderWidth: 0,
        backgroundColor: 'rgba(250, 235, 204, 0.5)',
        data: [{x: 0, y: MEJA_PRENIZKE_TEZE}, {x: bmiFormat.datum[bmiFormat.datum.length - 1], y: MEJA_PRENIZKE_TEZE}]
      },
      {
        fill: '-1',
        label: "Normalna teža",
        pointRadius: 0,
        borderWidth: 0,
        backgroundColor: 'rgba(60, 118, 61, 0.2)',
        data: [{x: 0, y: MEJA_PREKOMERNE_TEZE}, {x: bmiFormat.datum[bmiFormat.datum.length - 1], y: MEJA_PREKOMERNE_TEZE}]
      },
      {
        fill: '-1',
        label: "Previsoka teža",
        pointRadius: 0,
        borderWidth: 0,
        backgroundColor: 'rgba(169, 68, 66, 0.2)',
        data: [{x: 0, y: MEJA_DEBELOSTI}, {x: bmiFormat.datum[bmiFormat.datum.length - 1], y: MEJA_DEBELOSTI}]
      }]
    },
    options: {
      tooltips: {
          // Disable the on-canvas tooltip
          enabled: false,

          custom: function(tooltipModel) {
              // Tooltip Element
              var tooltipEl = document.getElementById('chartjs-tooltip');

              // Create element on first render
              if (!tooltipEl) {
                  tooltipEl = document.createElement('div');
                  tooltipEl.id = 'chartjs-tooltip';
                  tooltipEl.innerHTML = '<table></table>';
                  document.body.appendChild(tooltipEl);
              }

              // Hide if no tooltip
              if (tooltipModel.opacity === 0) {
                  tooltipEl.style.opacity = 0;
                  return;
              }

              // Set caret Position
              tooltipEl.classList.remove('above', 'below', 'no-transform');
              if (tooltipModel.yAlign) {
                  tooltipEl.classList.add(tooltipModel.yAlign);
              } else {
                  tooltipEl.classList.add('no-transform');
              }

              function getBody(bodyItem) {
                  return bodyItem.lines;
              }

              // Set Text
              if (tooltipModel.body) {
                  var titleLines = tooltipModel.title || [];
                  var bodyLines = tooltipModel.body.map(getBody);

                  var innerHtml = '<thead>';

                  titleLines.forEach(function(title) {
                      innerHtml += '<tr><th>' + title + '</th></tr>';
                  });
                  innerHtml += '</thead><tbody>';

                  bodyLines.forEach(function(body, i) {
                      var colors = tooltipModel.labelColors[i];
                      var style = 'background:' + colors.backgroundColor;
                      style += '; border-color:' + colors.borderColor;
                      style += '; border-width: 2px';
                      var span = '<span style="' + style + '"></span>';
                      innerHtml += '<tr><td>' + span + tezaFormat.stevilke[i] + ' kg</td></tr>';
                  });
                  innerHtml += '</tbody>';

                  var tableRoot = tooltipEl.querySelector('table');
                  tableRoot.innerHTML = innerHtml;
              }

              // `this` will be the overall tooltip
              var position = this._chart.canvas.getBoundingClientRect();

              // Display, position, and set styles for font
              tooltipEl.style.opacity = 1;
              tooltipEl.style.position = 'absolute';
              tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
              tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
              tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
              tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
              tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
              tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
              tooltipEl.style.pointerEvents = 'none';
          }
      }
    }
  });
}

$(document).ready(function() {

  /* master - detail */
  $(".panel").each(function() {
    var hidden = true;
    $(this).find(".panel-heading").click(function() {
      if (hidden) {
        $(this).parent().find(".vsebina").show();
        hidden = false;
      }
      else {
        $(this).parent().find(".vsebina").hide();
        hidden = true;
      }
    });
  });
});
